
import sys
import random
import numpy as np

def shuffle_column(column, *random_indices):
   """Randomly shuffle a column.

   For debugging purposes you can pass a list that will be used as
   the new index for this column
   """

   if random_indices:
      shuffled = [column[random_indices[i]] for i in range(len(column))]
   else:
      shuffled = random.sample(column, len(column))

   for i in range(column.size):
      column[i] = shuffled[i]

def shuffle(mat, *random_indices):
   """Randomly shuffle each column.

   For debugging purposes you can pass a list that will be used as
   the new index _for each column_
   """

   for j in range(mat.shape[1]):
      shuffle_column(mat[:,j], *random_indices)



# done in Alignment class now
#def strains_mat(strains):
#   """Makes a numpy matrix of strains.
#
#   If there are 7 genes and n strains we'll make
#
#      [ a0  b0  c0  d0  e0  f0  g0  ]
#      [ a0' b0' c0' d0' e0' f0' g0' ]
#      [  .   .   .   .   .   .   .
#      [  .   .   .   .   .   .   .
#      [ an  bn  cn  dn  en  fn  gn  ]
#      [ an' bn' cn' dn' en' fn' gn' ]
#
#   where (ai, ai') is the haplotype of the ith strain.
#
#   """
#
#   if len(strains) < 1:
#      return
#
#   height = sum([s.height for s in strains])
#   width = strains[0].len
#   matrix = np.empty((height, width), dtype=np.uint8)
#
#   row = 0
#   for s in strains:
#      col = 0
#      for hap in s:
#         for i, allele in enumerate(hap):
#            matrix[row + i, col:(col+hap.len)] = allele
#         col += hap.len
#       
#      row += s.height
#
#   return matrix
#
#
#   
#
