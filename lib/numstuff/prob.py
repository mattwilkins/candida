
import math

eri_cache = {}
fact_cache = { 0:1 }
combination_cache = {}

def fact(n):
   """Factorial."""
   if type(n) is not int or n < 0:
      raise ValueError, "factorial of " + str(n) + " makes no sense"
  
   if n not in fact_cache:
      fact_cache[n] = n * fact(n-1)
   return fact_cache[n]

def combination(n, k):
   """Chose k from n, order doesn't matter."""
   if n < k:
      return 0
   
   if (n, k) not in combination_cache:
      combination_cache[(n, k)] = fact(n) / (fact(k) * fact(n - k))
   return combination_cache[(n, k)] 

def possible_patterns(n):
   """Possible triples for n."""

   pat = []
   for a in range(n+1):
      for b in range(n+1):
         c = n - a - b
         if a >= b >= c >= 0:
            pat.append([a,b,c])
   return pat

def number_of_patterns(a, b, c):
   """Number of patterns (a, b, c).  a >= b >= c."""

   n = a + b + c
   result = combination(n, b) * combination(n - b, c)
   if ( a != b != c ):
      result *= 6
   elif ( a != b or b != c ):
      result *= 3
   return result

def unnormalized_recomb_index(a, b, c):
   """The unnormalized index.

   Must have a >= b >= c
   """

   n = a + b + c
   return float(b + c) / n

def expected_recomb_index(n):
   """Expected Recombination Index for given n."""

   if n in eri_cache:
      return eri_cache[n]

   eri = 0
   for p in possible_patterns(n):
      eri += unnormalized_recomb_index(*p) * number_of_patterns(*p) / math.pow(3, n)
   eri_cache[n] = eri
   return eri

