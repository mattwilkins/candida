
# WARNING: THIS IS NOT USED BY THE CANDIDA CODE, MOVED TO NUMPY ON 17
# AUGUST 2009.  MAY NOT BE MAINTAINED

import random


class Oldmatrix:
   """A Matrix."""

   def __init__(self, data = None, fp = None):
      """Form a matrix in row order.

      data     a list (just one row) or a list of lists.
      fp       option fp to read data from.  if fp is past, then data
               argument is ignored

      if data is one row we will store [data] (so it will be like the
      second case - a list of lists)
      """

      # read in from file
      if fp is not None:
         self.__data = []
         for line in fp:
            self.append(line.split())
         return

      
      if data is None:
         self.__data = []

      else: 

         if type(data) is not list:
            raise ValueError, "data must be a list"

         # just one row
         if type(data[0]) is not list:
            self.__data = [data]
         else:
            # check equal length rows
            le = len(data[0])
            for d in data:
               if len(d) != le:
                  raise ValueError, "rows not same length"
            self.__data = data

   def shuffle_column(self, j, *random_indices):
      """Randomly shuffle a column.
      
      For debugging purposes you can pass a list that will be used as
      the new index for this column
      """
 
      column = self.get_col(j)
      if random_indices:
         shuffled = [column[random_indices[i]] for i in range(len(column))]
      else:
         shuffled = random.sample(column, len(column))

      for i in range(self.nrows()):
         self.__data[i][j] = shuffled[i]
   
   def shuffle(self, *random_indices):
      """Randomly shuffle each column.
      
      For debugging purposes you can pass a list that will be used as
      the new index _for each column_
      """
 
      for j, column in self.itercols():
         self.shuffle_column(j, *random_indices)
   
   def append(self, row):
      if type(row) is not list:
         raise ValueError, "row must be a list"
      if self.ncols() != 0 and len(row) != self.ncols():
         raise ValueError, "row is wrong length"
      self.__data.append(row)

   def __iter__(self):
      for row in self.__data:
         yield row

   def iterrows(self):
      for i, row in zip(range(self.nrows()), self.__data):
         yield i, row

   def itercols(self):
      for j, col in zip(range(self.ncols()), zip(*self.__data)):
         yield j, col

   def __getitem__(self, k):
      """A row."""
      return self.__data[k]

   def get_col(self, k):
      """A col."""
      return zip(*self.__data)[k]

   def ncols(self):
      if self.__data == []:
         return 0
      return len(self.__data[0])

   def nrows(self):
      if self.__data == []:
         return 0
      return len(self.__data)

   def list(self):
      """As a list of lists."""
      return [row for row in self.__data]         

   #def str(self):
   #   return reduce(lambda x, y: x + '\n' + ' '.join(y), self.__data, '')

