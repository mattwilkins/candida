
import os, sys, re
import subprocess
import tempfile
import misc.common
import numpy as np
from quartet.allele import Allele


def varscore_to_mutationrate(vs):
   """Return the mutation rate for a given varscore."""
   return 5e-10 * np.exp(1.4*vs)

def mutationrate_to_varscore(r):
   """Return the varscore for a given mutationrate."""
   return np.log(r/5e-10) / 1.4

def varscores_old(allele):
   """A generator of Varscore for given allele."""

   # get a string representation of allele
   s = allele.str()

   cmd = "printf \">0\n%s\n\" | /awc/local/bin/repeat_chars -sn" % s 
   p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   (stdout, stderr) = p.communicate()
   
   if stdout == 'no repeat signature\n':
      return 

   cmd = "printf \"%s\" | /awc/local/bin/varscore.pl -s" % stdout
   p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   (stdout, stderr) = p.communicate()

   for line in stdout.split('\n')[:-1]:
      (num, unit_size, purity, number_of_units, variability, seq) = line.split()
      yield Varscore(unit_size, purity, number_of_units, variability, seq)

def varscores(alleles):
   """Return number of alleles with no varscore, and a list of Varscores."""

   s = '\n>0\n'.join(map(lambda x: x.str(), alleles))

   f = tempfile.NamedTemporaryFile(delete=False)
   f.write('>0\n%s\n' % s)
   f.close()
   cmd = "/awc/local/bin/repeat_chars -sn %s" % f.name
   p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   (stdout, stderr) = p.communicate()
   os.unlink(f.name)

   varscores = []

   # split into no repeat lines, and other lines
   novarscores = stdout.count('no repeat signature')
   stdout =  re.sub('no repeat signature\n', '', stdout)

   if stdout:
      f = tempfile.NamedTemporaryFile(delete=False)
      f.write(stdout)
      f.close()
      #cmd = "printf \"%s\" | /awc/local/bin/varscore.pl -s" % stdout
      cmd = "/awc/local/bin/varscore.pl -s %s" % f.name
      p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
      (stdout, stderr) = p.communicate()
      os.unlink(f.name)

      for line in stdout.split('\n')[:-1]:
         (num, unit_size, purity, number_of_units, variability, seq) = line.split()
         varscores.append(Varscore(unit_size, purity, number_of_units, variability, seq))

   return (novarscores, varscores)

class Varscore(object):

   def __init__(self, unit_size, purity, number_of_units, variability, seq):
      """Form a varscore."""

      self.__unit_size = int(unit_size)
      #self.purity = int(purity)
      self.purity = purity    # used to be just an int, but now allow a string.  This means we can handle multiple purities, which we need since a bunch of varscores is just a varscore, but will have multiple purities, one for each individual sequence/varscore
      self.number_of_units = float(number_of_units)
      self.variability = float(variability)
      self.sequence = Allele(seq)

   # this is just because i wanted to figure this out, we could easily
   # just put self.__unit_size as a public field self.unit_size

   @property 
   def unit_size(self):
      #print "In the us getter"
      return self.__unit_size

   @unit_size.setter
   def unit_size(self, us):
      #print "In the setter"
      self.__unit_size = us

   def str(self):
      return "unit_size = %d, purity = %s, number_of_units = %f, var = %f, seq = %s" % (self.unit_size, self.purity, self.number_of_units, self.variability, self.sequence.str())

def bunchup(allele, varscores):
   """Return a set of sets of varscores.

   Each set of varscores is made up of overlapping varscores.  Not all
   the varscores have to overlap each other, it isn't an intersection,
   rather a union of varscores.

   """

   if len(varscores) == 0:
      return []

   # we need to know where each varscore is
   for v in varscores:

      start = allele.str().find(v.sequence.str())
      length = len(v.sequence)
  
      v.start = start
      v.length = length
      v.coverage = set(xrange(start, start+length))

   # they all start unbunched
   unbunched = set(varscores)

   # a list of bunches
   bunches = []

   while unbunched:

      # we will move pick and all its overlaps over into a set in
      # bunches
      pick = unbunched.pop()
      currentbunch = set([pick])
      currentbunchcoverage = pick.coverage
      bunches.append(currentbunch)

      # repeatedly loop through unbunched pulling all we can into
      # currentbunch
      while True:

         newunbunched = set([])
         donemv = False
         for new in list(unbunched):

            if currentbunchcoverage & new.coverage: # there is some overlap
               currentbunch.add(new)
               currentbunchcoverage = currentbunchcoverage | new.coverage
               donemv = True
            else:
               newunbunched.add(new)
   
         unbunched = newunbunched
         if not donemv:    # didn't move any from unbunched to currentbunch
            break
   
   return bunches

def bunchvarscore(varscores):
   """Return a varscore for a bunch of varscores."""

   # find start index
   start = min([v.start for v in varscores])
   end = max([v.start + v.length for v in varscores])

   # the union of the sequences, and a score at each index
   scores = []
   seq = []

   for index in range(start, end):

      # get the scores at this index, and the nucleotide
      s = []
      nucs = []
      for v in varscores:
         if index in v.coverage:
            s.append(v.variability)
            nucs.append(v.sequence[index - v.start])


      # check that nucs is constant
      if not misc.common.const(nucs):
         raise ValueError, "Internal error: at index %d, we have different nucleotides: %s" % (index, str(nucs))

      scores.append(float(sum(s)) / len(s))
      seq.append(nucs[0])

   # convert the list of nucleotides to a string
   seq = misc.common.np2str(seq)

   #for v in varscores:
   #   print "score = %0.2f, seq = %s" % (v.variability, v.sequence.str())

   #print "score = %0.2f, seq = %s" % (float(sum(scores)) / len(scores), seq)
   #print

   #return Varscore(0, 0, 0, float(sum(scores)) / len(scores), seq)
   return Varscore(0, ':'.join([v.purity for v in varscores]), 0, float(sum(scores)) / len(scores), seq)

def bunchvarscore_useweightedmax(varscores):
   """Return a varscore for a bunch of varscores.

   Not the average of the average (like bunchvarscore()), rather the varscore is defined as:

   	Sum up all the individual varscores, except where the sequences overlap. 
	At the overlaps use the maximum varscore
 	Weight this by how much of the sequence is used

   An easy way to do this is like bunchvarscore() but at each index use the max over all the sequences at that index normalized by the length
   """

   # find start index
   start = min([v.start for v in varscores])
   end = max([v.start + v.length for v in varscores])

   # the union of the sequences, and a score at each index
   scores = []
   seq = []

   for index in range(start, end):

      # get the scores at this index, and the nucleotide
      s = []
      nucs = []
      for v in varscores:
         if index in v.coverage:
            s.append((v.variability, v.length))
            nucs.append(v.sequence[index - v.start])


      # check that nucs is constant
      if not misc.common.const(nucs):
         raise ValueError, "Internal error: at index %d, we have different nucleotides: %s" % (index, str(nucs))

      # the largest varscore and the length of the sequence it comes from
      ml = max(s,key=lambda x: x[0])
      scores.append(varscore_to_mutationrate(ml[0]) / ml[1])
      seq.append(nucs[0])

   # convert the list of nucleotides to a string
   seq = misc.common.np2str(seq)

   #print "Tot=", sum(scores)

   return Varscore(0, ':'.join([v.purity for v in varscores]), 0, mutationrate_to_varscore(float(sum(scores))), seq)

