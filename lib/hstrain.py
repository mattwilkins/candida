
import os
import sys
from pysvg.builders import ShapeBuilder
import pysvg.text

import copy

#findbin = os.path.dirname(os.path.realpath(sys.argv[0]))
#sys.path.append(findbin + '/../code/lib')

from quartet.strain import Strain
from quartet.haplotype import Haplotype
from quartet.allele import Allele
import misc.common

class HStrain(Strain):
   """A hierarchical strain with kids."""

   square_size = 20
   hpad = 5
   vpad = 50
      
   def __init__(self, name, haps):

      # if haps is a string, do the conversion into a list of Haps
      if type(haps) is str:
         if haps == '':
            raise ValueError, "Can't have empty string for HStrain"
         haps = [Haplotype([Allele(haps)])]
      Strain.__init__(self, name, haps)
      self.__kids = []    
   
   def set_kids(self, kids):
      self.__kids = kids

   def append_kid(self, kid):
      self.__kids.append(kid)
   
   def get_kids(self):
      return self.__kids

   def kids(self):
      for k in self.__kids:
         yield k

   def ancestor_of(self, names):
      """Me in names, or an ancestor of something in names."""
      if self.get_name() in names:
         return True
      else:
         for k in self.__kids:
            if k.ancestor_of(names):
               return True
      return False

   def prune(self, leafs):
      """Remove everything except the tree supporting the strains with given names."""

      # it is an error if i have to remove myself
      if not self.ancestor_of(leafs):
         raise ValueError, "I am not needed in the supporting tree for leafs"

      # remove my kids that aren't needed
      newkids = []
      for k in self.__kids:
         if k.ancestor_of(leafs): newkids.append(k)
      self.set_kids(newkids)

      # now let my remaining kids prune themselves
      for k in self.__kids:
         k.prune(leafs)

   def rmdups(self):
      """Remove parents that have one child that is identical to themselves."""

      # while i have one identical kid - don't drop last one in chain
      #while len(self.__kids) == 1 and self.str() == self.__kids[0].str():
      while len(self.__kids) == 1 and self.str() == self.__kids[0].str() and self.__kids[0].get_kids():
         self.set_kids(self.__kids[0].get_kids())
     
      for k in self.__kids:
         k.rmdups()

   def cull_constant(self):
      """Remove nucleotides in me and children, which are identical.
    
      If a site in me and all children is identical (eg first site is
      a T), then toss it out shrinking me and all children.  Do this
      for every site.

      """

      def __common_nuc(self, i):
         """The unique nucleotide at this position and all kids or -1."""
         
         # get kids common nucs
         kidsnuc = None
         for k in self.__kids:
            a = __common_nuc(k,i)
            if not kidsnuc: kidsnuc = a   # first kid
            if a != kidsnuc:
               return -1                  # kids have different nucleotides
        
         me = self.str()[i]
         if not kidsnuc or me == kidsnuc:    # no kids, or same nuc as me
            return me
         else:
            return -1
      

      tokeep = []
      for i in range(self.size):
         if __common_nuc(self,i) == -1:
            # keep this nuc since not common
            tokeep.append(i)

      self.rmnucs(tokeep)

   def rmnucs(self, tokeep):
      """Keep nucleotides at positions in tokeep in me and children."""

      # build my string
      str = self.str()
      newstr = []
      for i in sorted(tokeep):
         newstr.append(str[i])
      self.put_hap(0, Haplotype([Allele(newstr)]))

      # fix my length
      self.size = len(newstr)

      # do my kids
      for k in self.__kids:
         k.rmnucs(tokeep)


   def render(self, svg, xhash, y):
      """Display me and kids into svg with a more regular box shape.

      xhash maps y value to what x value we are up to on this line, so
      the kids just stack one beside each other

      """

      oh = ShapeBuilder()

      def __square(x, y, col):
         """Put a square at (x, y) with given colour."""
         return oh.createRect(x, y, HStrain.square_size, HStrain.square_size, stroke='black',fill=col)
     
      # first me, top left at (x, y)
      if y not in xhash: xhash[y] = 0                          # start at 0 on this line
      mid = xhash[y] + (HStrain.square_size * self.size)/2     # need this later for drawing connecting lines
      svg.addElement(pysvg.text.text(self.get_name(), xhash[y], y))
      for s in self.str():
         if s == 'A':
            svg.addElement(__square(xhash[y], y, 'red'))
         elif s == 'C':
            svg.addElement(__square(xhash[y], y, 'green'))
         elif s == 'G':
            svg.addElement(__square(xhash[y], y, 'yellow'))
         elif s == 'T':
            svg.addElement(__square(xhash[y], y, 'blue'))
         xhash[y] += HStrain.square_size

      # need a gap between me and a sibling on this line
      xhash[y] += HStrain.hpad

      # now kids
      for k in self.__kids:
         k.render(svg, xhash, y + HStrain.vpad)
         # connect to kids
         svg.addElement(oh.createLine(mid, y+HStrain.square_size, xhash[y + HStrain.vpad] - HStrain.hpad - (HStrain.square_size * k.size)/2, y+HStrain.vpad))

