
import os
import sys
import re
import pyparsing as pp
import string

#findbin = os.path.dirname(os.path.realpath(sys.argv[0]))
#sys.path.append(findbin + '/..')
#sys.path.append(findbin + '/../../code/lib/')

from hstrain import HStrain


def parse(str):
   """Parse a string into a heirachy of strains.

   For example
      ((TTG,CGC)TTG,((CAT,()GCC)CAT,TTG)CAT)TTG
   """

   lpar = pp.Suppress( "(" )
   rpar = pp.Suppress( ")" )
   leaf = pp.Word(pp.alphanums + '.').setParseAction( lambda s, l, t: t[0] )
   
   # Need to forward declare this due to circularity
   node_list = pp.Forward()
   tree = ( node_list + leaf  ).setParseAction( lambda s, l, t: [t.asList()] ) | leaf
   node_list << ( lpar + pp.delimitedList( tree ) + rpar ).setParseAction( lambda s, l, t: [ t.asList() ] )

   return __parseit(tree.parseString(str).asList()[0])
   
def __parseit(remainder):

   # no children
   if type(remainder) is str:
      (name, data) = string.split(remainder, '.') 
      return HStrain(name, data)
  
   # [ subtree_list, parent ]
   (name, data) = string.split(remainder[1], '.') 
   hs = HStrain(name, data)

   for s in remainder[0]:
      hs.append_kid(__parseit(s))

   return hs


def stringify(hstrain):
   """Print out a newick format."""

   strings = []
   for k in hstrain.kids():
      strings.append(stringify(k))

   if strings:
      return '(' + ','.join(strings) + ')' + hstrain.get_name() + '.' + hstrain.str()
   else:
      return hstrain.get_name() + '.' + hstrain.str()

