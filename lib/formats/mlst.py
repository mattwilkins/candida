
import os
import sys
from quartet.allele import Allele
from quartet.haplotype import Haplotype
from quartet.strain import Strain
import misc.common


class Mlst:
   """A Mlst reader."""

   def __init__(self, strains_file, legacy=True):
      """Read in relaxed MLST strains, or Phylip format.

      If legacy is False then we should return a bunch of Alleles when
      reading phylip, not Strains, since Strains are overkill for
      simple phylip.  However since we have been using Strains up to
      this point, for now this requires an override.
      
      """

      # if first line contains :, then it is mlst, else phylip
      f = open(strains_file)
      firstline = f.readline()
      f.close()
      if firstline.find(':') == -1:
         self.__read_phylip(strains_file, legacy)
      else:
         self.__read_mlst(strains_file)

   def __read_phylip(self, strains_file, legacy):
      """Read in a phylip format.
      
      If legacy is False then we should return a bunch of Alleles, not
      Strains, since Strains are overkill for simple phylip.  However
      since we have been using Strains up to this point, for now this
      requires an override.
      
      """
      sf = open(strains_file)
      sf.readline()     # don't need the number of strains, or lengths
     
      self.__strains = []
      self.__genenames = []
      for line in sf:
         (name, allele) = line.split()
         allele = allele[0: 3 * (len(allele)/ 3)]
         if legacy: 
            self.__strains.append(Strain(name, [Haplotype([Allele(allele)])]))
         else:
            self.__strains.append(Allele(allele, name=name))
      sf.close()

   def __read_mlst(self, strains_file):
      """Read in a MLST format."""

      # first line of the strains_file is:
      # genename0:orfshift0 genename1:orfshift1 ...
      sf = open(strains_file)
      self.__genenames = genenames = []
      orf = {}
      for gs in sf.readline().split():
         [g, s] = gs.split(':')
         genenames.append(g)
         orf[g] = int(s)

      # check orfs
      for o in orf.values():
         if (type(o) is not int) or orf < 0:
            raise ValueError, "Orf given is not a non negative integer"
         
      # maps gname to { hapnum : hap }
      genes = {}
      for gname in genenames:
         haps = {} # hap number to hap for this particular gene
         
         # check all alleles are same length
         allele_lengths = []

         # gene file name is realitive to strains_file
         for line in open(os.path.join(os.path.dirname(strains_file), gname)):
            hap, allele_strings = line.split()[0], line.split()[1:]
            if int(hap) in haps:
               raise ValueError, "Gene " + gname + " already seen haplotype " + hap

            # do orf, and make multiple of 3 in length
            alleles = []
            for a in allele_strings:

               # while here, check that a is made up of A, C, G, or T
               for x in a:
                  if x not in ['A', 'C', 'G', 'T']:
                     raise ValueError, 'Datum ' + x + ' for allele is not A, C, G, or T'

               a = a[orf[gname]:]
               alleles.append(Allele(a[0: 3 * (len(a)/ 3)]))
            if len(alleles) == 1:   # actually only one allele so don't need haplotype
               haps[int(hap)] = alleles[0]
            else:
               haps[int(hap)] = Haplotype(alleles)

            # store all allele lengths so can check same
            allele_lengths.extend([ len(a) for a in alleles ])

         if not misc.common.const(allele_lengths):
            raise ValueError, "Gene " + gname + " alleles have different lengths"
         
         genes[gname] = haps


      # read strains, already got the first line genenames and orfs
      self.__strains = []
      for line in sf:
         values = line.split()
         strain_name = values.pop(0)
         haps = [genes[gname][int(val)] for gname, val in zip(genenames, values)]
         self.__strains.append(Strain(strain_name, haps))
      sf.close()

      self.__genes = genes

   def get_strains(self):
      return self.__strains

   def get_hap(self, gname):
      return self.__genes[gname]

   def get_genenames(self):
      return self.__genenames

   @staticmethod
   def phylip_format(sl):
      """Produce a phylip format string."""
      array = []
   
      if len(sl) <= 0:
         return
      
      array.append('%s %s' % (len(sl), sl[0].size))
      for i, s in enumerate(sl):
         array.append('%-10s%s' % (s.get_name(), s.str()))

      return '\n'.join(array)

   def phylip(self):
      """A phylip representation of me."""
      return Mlst.phylip_format(self.__strains)

   def variability(self, gname):
      """A list containing number of sites with different codons.

      Element i in the list is the number of third-codon sites that
      have i different codons.  So in practice with only A, C, G, and
      T the list will be 4 in length

      This has been superseded by the h_variablity stuff which reads
      in the strains and operates directly on those

      """

      haps = self.__genes[gname]

      matrix = []
      for k in sorted(haps.keys()):
         for a in haps[k]:
            matrix.append(a)

      # array of sets
      codons = []
      for j in range(0, len(matrix[0]), 3):
         column =  [matrix[i][j] for i in range(len(matrix))]

         # store the codons seen at this site
         s = set()
         codons.append(s)
         for c in column:
            s.add(c)

      # now count up the
      num = [0, 0, 0, 0, 0]
      for codon_set in codons:
         num[len(codon_set)] += 1

      num.pop(0)
      return num



