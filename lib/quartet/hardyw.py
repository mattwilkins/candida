
import math
import numpy as np
import numstuff.mynumpy

def g_test(strains_mat):
   """Works out G for each column in the matrix.

   For HW proportions the number of degrees of freedom is num of
   genotypes - num of alleles, ie 10 - 2 = 8

   Using the table at
   http://www.itl.nist.gov/div898/handbook/eda/section3/eda3674.htm
   We see that nu = 8, so the for a chi distribution the there is only
   a 5% chance of exceeding 15.507.  We will use 15.507 then.

                  Probability of exceeding the critical value
        nu           0.10      0.05     0.025      0.01     0.001
      
        1          2.706     3.841     5.024     6.635    10.828
        2          4.605     5.991     7.378     9.210    13.816
        3          6.251     7.815     9.348    11.345    16.266
        4          7.779     9.488    11.143    13.277    18.467
        5          9.236    11.070    12.833    15.086    20.515
        6         10.645    12.592    14.449    16.812    22.458
        7         12.017    14.067    16.013    18.475    24.322
        8         13.362    15.507    17.535    20.090    26.125
        9         14.684    16.919    19.023    21.666    27.877
       10         15.987    18.307    20.483    23.209    29.588
       11         17.275    19.675    21.920    24.725    31.264

   """

   ar = []
   for j, column in strains_mat.itercols():
      ar.append(g_test_col(column))
   return ar

def g_test_col(col):
   """Return G-test for a col."""

   # the number of times we see A, G, C, T, AA, AG, AC, AT, GG, etc
   num = num_of_nuc_pairs(col)

   G = 0.0
   for p in num:
      # ignore singletons (A, G, etc), and pairs which didn't appear
      if len(p) == 1 or num[p] == 0 : continue

      # E(AA) = E(A)   * E(A)   * (n/2)
      #       = N(A)/n * N(A)/n * (n/2)
      #       = N(A)*N(A)/2*n
      # E(AG) = 2 * E(A)   * E(G)   * (n/2)
      #       = 2 * N(A)/n * N(G)/n * (n/2)
      #       = N(A)*N(G)/n
      #
      # where E(A) is the prob that we see A in the col
      # the n/2 is because when looking at pairs the sample is half
      # the length
      x = p[0]
      y = p[1]
      expected = (num[x]*num[y])/(2.0 * len(col))
      if x != y: expected *= 2
      
      G += num[p] * math.log(num[p]/expected)

   return 2*G

def num_of_nuc_pairs(col):
   """The number of times nucleotides and pairs appear."""

   # The possible nucleotides
   nuc = ['A', 'G', 'C', 'T']

   # Possible pairs (note AG = GA, so don't store both)
   pairs = []
   for x in nuc:
      for y in nuc:
         if x <= y: pairs.append(x + y)

   # num maps nuc or pairs to times seen
   num = {}
   for n in nuc + pairs: num[n] = 0

   # go down column by twos
   for x,y in zip(col[0:len(col):2], col[1:len(col):2]):
      num[chr(x)] += 1
      num[chr(y)] += 1
      num[''.join(sorted([chr(x), chr(y)]))] += 1

   return num

