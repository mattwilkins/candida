
import numpy as np
import misc.common
from haplotype import Haplotype
import random

class Strain:
   """A Strain.
  
   A strain can be in two states
      a dictionary from gene name to haplotype
      collapsed: a string which consists of a concatenation of the
                  collapsed haplotypes

   
   """

   def __init__(self, name, haps):
      """Form a strain.

      name:          my name
      haps:          list of haps

      """
      
      self.__name = str(name)
      self.__haplotypes = haps
      self.size = sum([a.size for a in haps])

      if len(haps) > 0:
         self.__height = haps[0].get_height()
      else:
         self.__height = 0

   def collapse(self):
      """Ask each haplotype for one of its alleles."""
      return [h.get_allele() for h in self.__haplotypes]

   def get_ambiguity_sites(self):
      """Indices where ambiguities have occured."""

      ambig = []
      shift = 0
      for h in self.__haplotypes:
         ambig.extend(map(lambda (x): x+shift, h.get_ambiguities()))
         shift += h.size
      return ambig

   def copy(self):

      newhaps = [h.copy() for h in self.__haplotypes]
      return self.__class__(self.__name, newhaps)
      
   def mutate(self, prob, test = False):
      if type(prob) is list: # assume correct length, get an exception if not
         for i, h in enumerate(self.__haplotypes):
            h.mutate(prob[i], test)
      else:
         for h in self.__haplotypes:
            h.mutate(prob, test)

   @staticmethod
   def mating_patterns(width):
      """Produce all possible mating patterns up to given width."""
      return [misc.common.int2binary(i, width) for i in range(pow(2, width))]

   def mate(self, another, signature):
      """Produce a child strain from two strains.

      signature is a list consisting of booleans with
      len(signature) = self.get_num_haps() = another.get_num_haps()
      if the ith element of signature is True then the ith allele of
      the child will come from self, otherwise, another.
      
      """

      if self.size != another.size:
         raise ValueError, "Can't mate, different lengths"
      
      if self.__height != 1:
         raise ValueError, "Can't mate a diploid"

      alleles = []
      for i, s in enumerate(signature):
         if s:
            hap = self[i]
         else:
            hap = another[i]
         alleles.append(hap.copy())
      
      return Strain(0, alleles)

   def __getitem__(self, i):
      return self.__haplotypes[i]

   def copy_into(self, another):
      """Copy myself into another strain."""
      for a, b in zip(self, another):
         a.copy_into(b)

   def get_num_haps(self):
      return len(self.__haplotypes)

   def put_hap(self, i, hap):
      self.__haplotypes[i] = hap

   def __iter__(self):
      for h in self.__haplotypes:
         yield h

   def get_height(self):
      return self.__height

   def set_name(self, s):
      self.__name = str(s)

   def get_name(self):
      return self.__name

   def str(self):
      str = ''
      for a in self.collapse():
         str += ''.join(misc.common.np2nucs(a))
      return str
