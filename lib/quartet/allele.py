
import random
import numpy as np
import misc.common
import numstuff.varscore
import operator
import itertools

nuc_to_other_nucs = {}
for k, v in {'A' : ['C', 'G', 'T'], 'C' : ['A', 'G', 'T'], 'G' : ['A', 'C', 'T'], 'T' : ['A', 'C', 'G']}.iteritems():
   nuc_to_other_nucs[ord(k)] = map(ord, v)


class Allele(np.ndarray):
   """An allele, just a string of A, C, G, or T represented as ints."""

   def __new__(subtype, shape, name=None, dtype=float, buffer=None, offset=0, strides=None, order=None):
      #
      # this is boilplate ndarray, _except_ one can pass a string as
      # the shape, in which case that string is used as the buffer,
      # and the length of the string determines the shape.
      # this means we can do:
      #     a = Allele("ACGTCAGT")
      #
      if type(shape) is str:
         obj = np.ndarray.__new__(subtype, (len(shape),), np.uint8, np.array([ord(x) for x in shape],dtype=np.uint8), offset, strides, order)
      else:
         obj = np.ndarray.__new__(subtype, shape, dtype, buffer, offset, strides, order)

      obj.__name = name
      return obj

#   def __array_finalize__(self, obj):
#        # ``self`` is a new object resulting from
#        # ndarray.__new__(InfoArray, ...), therefore it only has
#        # attributes that the ndarray.__new__ constructor gave it -
#        # i.e. those of a standard ndarray.
#        #
#        # We could have got to the ndarray.__new__ call in 3 ways:
#        # From an explicit constructor - e.g. InfoArray():
#        #    obj is None
#        #    (we're in the middle of the InfoArray.__new__
#        #    constructor, and self.info will be set when we return to
#        #    InfoArray.__new__)
#        if obj is None: return
#        # From view casting - e.g arr.view(InfoArray):
#        #    obj is arr
#        #    (type(obj) can be InfoArray)
#        # From new-from-template - e.g infoarr[:3]
#        #    type(obj) is InfoArray
#        #
#        # Note that it is here, rather than in the __new__ method,
#        # that we set the default value for 'info', because this
#        # method sees all creation of default objects - with the
#        # InfoArray.__new__ constructor, but also with
#        # arr.view(InfoArray).
#        #self.__name = getattr(obj, '__name', None)
#        #self.len    = getattr(obj, 'len', None)
#        #self.height = getattr(obj, 'height', 1)


   def get_height(self):
      return 1

   def get_ambiguities(self):
      """So can be used in place of a haplotype."""
      return []

   def get_allele(self):
      """So can be used in place of a haplotype."""
      return self

   def get_alleles(self):
      """So can be used in place of a haplotype."""
      return [self]

   def get_name(self):
      # since array_finalize is empty (for speed reasons), we might
      # not have a name
      if '_Allele__name' in self.__dict__:
         return self.__name
      else:
         return None

   def set_name(self, name):
      self.__name = name

   def copy_into(self, other):
      other[:] = self

   def str(self):
      return ''.join(misc.common.np2nucs(self))

   def mutate(self, prob, test = False):
      """Mutate oneself.

      if test is True, then nucleotides to mutate will be the first ones, and
      they will all mutate to A (65)

      """
      num_to_change = np.random.binomial(self.size, prob)
     
      if test:
         for i in range(int(round(prob * self.size))):
            self[i] = 65

      # 0 is a special case
      elif num_to_change == 0:
         return

      # this can be special too, although not sure if this makes much
      # difference speedwise, definitely randint is faster than
      # random.sample(blah,1), but how often is this called?
      elif num_to_change == 1:
         i = random.randint(0, self.size-1)
         self[i] = random.choice(nuc_to_other_nucs[self[i]])

      else:
         ind = random.sample(xrange(self.size), num_to_change)
         for i in ind:
            self[i] = random.choice(nuc_to_other_nucs[self[i]])

   #
   # this and below is all for varscores
   #
   def varscores(self):
      (no, varscores) = numstuff.varscore.varscores([self])
      return varscores

   def permute(self, aagen, aa2codons, onecodon = False):
      """Return a random permutation that maps to same aa.
   
      aagen is an Aagen
      aa2codons maps aa to a set of codon strings
      if onecodon then only use one codon per aa
      """


      # maps aa to the picked codon
      aa2picked = {}
      def pick_a_codon(a):
         if a not in aa2picked:
            aa2picked[a] = random.choice(list(aa2codons[a]))
         return aa2picked[a]

  
      # go thru us codon by codon
      codons = []
      for i in range(0, len(self), 3):
         codon = self[i:i+3]
         #print "Looking at codon", codon
         aa = aagen.amino_acids(codon)
         #print "Its aa is", aa
         #print "Possible codonds to use are", aa2codons[aa]
         if onecodon:
            codons.append(pick_a_codon(aa))
         else:
            codons.append(random.choice(list(aa2codons[aa])))
   
      return Allele(''.join(codons))
  
   def allonecodonpermutations(self, aagen, aa2codons):
      """Return all the onecodon permutations."""

      # aas is an array of the aa's, and poss is an array of possible
      # sets
      aas = aa2codons.keys()
      poss = aa2codons.values()
  
      # this is our aa seq
      aa = self.aa(aagen)

      sequences = []
      for val in itertools.product(*poss):

         # this maps aa to our chosen codon
         m = dict([(a, p) for a, p in zip(aas, val)])
         sequence = map(m.get, aa)

         sequences.append(Allele(''.join(sequence)))

      return sequences

   def aa(self, aagen):
      """Return the aa string."""
      aa = []
      for i in range(0, len(self), 3):
         codon = self[i:i+3]
         aa.append(aagen.amino_acids(codon))
      return ''.join(aa)

   def num_of_permutations(self, aagen, aa2codons, onecodon = False):
      """Return the maximal possible number of permutations."""

      aa = self.aa(aagen)
      if onecodon:
         # dict (not list like aa is) from aminoacid to set of codons
         aareduced = dict([(a, aa2codons[a]) for a in aa])
         aa = aareduced.keys()
      return reduce(operator.mul, map(lambda (x): len(aa2codons[x]), aa))


