
import misc.common
import numpy as np
import re
from alignment import Alignment



class Quartet:
   """Four strains or isolates."""

   def __init__(self, strains):
      """Form a quartet."""

      if len(strains) != 4:
         raise ValueError, "Need four strains for quartet"

      self.__strains = strains
      self.__alignment = Alignment(strains, True)

      # must reclassify sites since strains changed
      self.__classified = False

   def rm_noisy_codons(self, aagen):
      """Remove codons in each strain that map to different aminoacids."""

      self.__alignment.rm_noisy_codons(aagen)

#   def rm_noisy_codons_and_ambiguities(self, aagen):
#
#      # array that is True at silent codons
#      silents = self.__get_silent_codons(aagen)
#
#      # FIXME, clumsy a set with indices where ambiguities occur
#      ambigs = set()
#      for s in self.__strains:
#         ambigs.update(set(s.get_ambiguity_sites()))
#
#      # FIXME, clumst convert ambigs to a 
#      mask = []
#      for i in range(len(silents)):
#         if silents[i] == False or (i in ambigs):
#            mask.append(False)
#         else:
#            mask.append(True)
#     
#      self.__alignment = np.compress(mask, self.__alignment, axis=1)
      
   def recomb_index(self):
      """The Recombination Index."""

      if not self.__classified:
         self.__classify_sites()

      lengths = map(len, self.__classified_sites[:3])
      lengths.sort()

      s = sum(lengths)
      if s == 0:
         return -1
      else :
         return float(lengths[0]+lengths[1]) / s


   def __distances_allsites(self):
      """Distances between strains looking at all sites."""

      dist = [0, 0, 0, 0, 0, 0]
      for j in range(self.__alignment.shape[1]):
         column = self.__alignment[:,j]

         if column[0] != column[1]:
            dist[0] += 1
         if column[0] != column[2]:
            dist[1] += 1
         if column[0] != column[3]:
            dist[2] += 1
         if column[1] != column[2]:
            dist[3] += 1
         if column[1] != column[3]:
            dist[4] += 1
         if column[2] != column[3]:
            dist[5] += 1
   
      return dist
     
   def __distances(self):
      """Distances between strains only looking at uninformative sites."""

      if not self.__classified:
         self.__classify_sites()

      # matrix = [
      #     [A,C,T,A,A...]
      #     [T,C,T,G,A...]
      #     [A,G,T,A,A...]
      #     [C,C,T,C,A...]
      #           ]
      dist = [0, 0, 0, 0, 0, 0]
      for i in self.__classified_sites[-1]:
         column = self.__alignment[:,i]

         if column[0] != column[1]:
            dist[0] += 1
         if column[0] != column[2]:
            dist[1] += 1
         if column[0] != column[3]:
            dist[2] += 1
         if column[1] != column[2]:
            dist[3] += 1
         if column[1] != column[3]:
            dist[4] += 1
         if column[2] != column[3]:
            dist[5] += 1
   
    
      return dist
     
   def divergence_age(self):
      """Min of dist between strains only looking at uninformative sites."""

      dist = self.__distances()
      dist.sort()
      return dist[0]
     
   def divergence_age_allsites(self):
      """Min of dist between strains looking at all sites."""

      dist = self.__distances_allsites()
      dist.sort()
      return dist[0]
     
   def divergence_age_oldest(self):
      """Max of dist between strains only looking at uninformative sites."""

      dist = self.__distances()
      dist.sort()
      return dist[-1]
     
   def divergence_age_mean(self):
      """Average of dist between strains only looking at uninformative sites."""

      dist = self.__distances()
      return 1.0 * sum(dist) / len(dist)
     
   def num_inf_sites(self):

      if not self.__classified:
         self.__classify_sites()

      return len(self.__classified_sites[0]) + len(self.__classified_sites[1]) + len(self.__classified_sites[2])

   def __classify_sites(self):
      """Split sites into the 3 informative and uniformative types.

      Creates four lists, the first contains sites that support AAGG,
      the second AGAG, the third AGGA, and the fourth are the
      uniformative sites

      """

      if self.__classified:
         return

      self.__classified_sites = [[],[],[],[]]

      for i in range(self.__alignment.shape[1]):
         column = self.__alignment[:,i]

         if column[0] == column[1] and column[0] != column[2] and column[2] == column[3]:
            self.__classified_sites[0].append(i) # AAGG
         elif column[0] != column[1] and column[0] == column[2] and column[1] == column[3]:
            self.__classified_sites[1].append(i) # AGAG
         elif column[0] != column[1] and column[0] == column[3] and column[1] == column[2]:
            self.__classified_sites[2].append(i) # AGGA
         else:
            self.__classified_sites[3].append(i)

      self.__classified = True


   def get_classified_sites(self):
      self.__classify_sites()
      return self.__classified_sites

   def __iter__(self):
      return self.__alignment.__iter__()
      #for row in self.__alignment:
      #   yield row

#   def list(self):
#      return [s.list() for s in self.__strains]

   #def str(self):
   #   if not self.__collapsed:
   #      raise ValueError, "Can't do str() before collapsing"
   #   return ' '.join([s.str() for s in self.__strains])


