
import numpy as np
import random

import misc.common
import ConfigParser

class Disambiguator:
   """Disambiguates alleles."""

   def __init__(self, a2nucs_filename):
      """Form a Disambiguator.

      a2nucs_filename:  the filename of the file that maps ambiguous
                        codes to a list of nucleotides

      """

      # dict from ambiguous to np.array of possibles
      self.__a2nucs = {}

      # this is a set of nucleotides that we don't disambiguate
      self.__leave = set()

      p = ConfigParser.SafeConfigParser()
      f = open(a2nucs_filename)
      p.readfp(f)
      for tup in p.items('a_to_nucs'):
         a = misc.common.nucs2np(tup[0].upper())[0]
         nucs = misc.common.nucs2np(tup[1].split())

         # deal with A: A type lines, these indicate that we should
         # leave these nucleotides alone
         if nucs.size == 1 and nucs[0] == a: self.__leave.add(a)

         self.__a2nucs[a] = nucs

      f.close()
      

   def dis_randomly(self, allele):
      """Disambiguate in a random fashion."""

      ambiguities = False
      for i, a in enumerate(allele):
         if a in self.__leave:
            continue
         elif a in self.__a2nucs:
            allele[i] = random.choice(self.__a2nucs[a])
            ambiguities = True
         else:
            raise ValueError, "Can't disambiguate " + chr(a) + " don't know what it should go to"

      return ambiguities

      
   def dis_all(self, allele):
      """Return a list of all possible alleles."""
      raise NotImplemented, "Can't disambiguate out to all alleles"


