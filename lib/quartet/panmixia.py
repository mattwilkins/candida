
import sys
from haplotype import Haplotype
from strain import Strain
from alignment import Alignment
from allele import Allele
import numstuff.mynumpy
import random
import numpy as np
import misc.common

def partitioned_shuffle(strains, *random_indices):
   """Limited shuffle on partitioned strains.

   strains is a list of lists of strains eg.
      [[s0, s4], [s3, s1, s7], [s10, s3], [s7]]

   shuffle each sublist with itself.

   """
   for s in strains:
      shuffle(s, *random_indices)
      #partial_shuffle(s, 0.1, *random_indices)
      
def shuffle(strains, *random_indices):
   """Panmixia shuffle strains.


   If there are 7 genes and n strains we'll make
      [ a0  b0  c0  d0  e0  f0  g0  ]
      [ a0' b0' c0' d0' e0' f0' g0' ]
      [  .   .   .   .   .   .   .
      [  .   .   .   .   .   .   .
      [ an  bn  cn  dn  en  fn  gn  ]
      [ an' bn' cn' dn' en' fn' gn' ]
   
   where (ai, ai') is the haplotype of the ith strain.

   Then shuffle each column, then put back the data into the strains.
   """

   #matrix = numstuff.mynumpy.strains_mat(strains)
   matrix = Alignment(strains)
   
   numstuff.mynumpy.shuffle(matrix, *random_indices)

   __matrix_into_strains(matrix, strains)

def partial_shuffle(strains, proportion, *random_indices):
   """Like panmixia shuffle, but only a few sites.

   If there are 7 genes and n strains we'll make
      [ a0  b0  c0  d0  e0  f0  g0  ]
      [ a0' b0' c0' d0' e0' f0' g0' ]
      [  .   .   .   .   .   .   .
      [  .   .   .   .   .   .   .
      [ an  bn  cn  dn  en  fn  gn  ]
      [ an' bn' cn' dn' en' fn' gn' ]
   
   where (ai, ai') is the haplotype of the ith strain.

   if proportion is a list, take it as the sites to involve in
   shuffling, else if it is a double take it as the proportion of
   sites to shuffle.

   Shuffle each column if its index appears in sites, then put
   back the data into the strains.
   """

   #matrix = numstuff.mynumpy.strains_mat(strains)
   matrix = Alignment(strains)

  
   # only shuffle a proportion of the columns
   if type(proportion) is list: 
      sites = proportion
   else:
      sites = random.sample(range(matrix.shape[1]), int(proportion * matrix.shape[1])) 
   for j in sites:
      numstuff.mynumpy.shuffle_column(matrix[:,j], *random_indices)

   __matrix_into_strains(matrix, strains)

def __matrix_into_strains(matrix, strains):
   """Rebuild the strains from the matrix."""

   # if strains are Alleles go row by row just making new Alleles
   if len(strains) > 0 and isinstance(strains[0], Allele):
      for row, i in zip(matrix, range(len(strains))):
         strains[i] = Allele(''.join(misc.common.np2nucs(row)))

   # go two rows at a time making new Haplotypes 
   else:
      for row, s in zip(range(0, matrix.shape[0], 2), strains):
         haps = []
         col = 0
         # length will be the length of the haplotypes for this strain
         for i, length in enumerate([hap.size for hap in s]):
            s.put_hap(i, Haplotype([matrix[row, col:(col+length)].view(Allele), matrix[row+1, col:(col+length)].view(Allele)]))
            col += length
   
   
