
import os
import sys
import ConfigParser


from quartet import Quartet
from formats.mlst import Mlst
from strain import Strain
from aagen import Aagen
from haplotype import Haplotype
import random
import panmixia

class Setup:
   """For reading in the inputs."""

   def __init__(self, strains_file, c2aa_file, mixia):
      """Read input files.
      
      mixia can be:
         None or False     don't mix
         True              perform panmixia on strains
         a filehandle      each line, containing space separated
                           integers, specifying a set of strains to
                           mix with each other
         
      """

      # codon_to_aa maps Codon to Amino Acid
      p = ConfigParser.SafeConfigParser() 
      f = open(c2aa_file)
      p.readfp(f)
      codon_to_aa = dict([(tup[0].upper(), tup[1]) for tup in p.items('codon_to_aa')])
      self.__aagen = Aagen(codon_to_aa)
      self.__codon_to_aa = codon_to_aa
      f.close()

      # get the strains
      m = Mlst(strains_file, legacy = False)
      self.__strains = m.get_strains()
      self.__genenames = m.get_genenames()

      # different types of mixia
      if mixia:
         if type(mixia) is file:
            substrains = []
            for line in mixia:
               substrains.append([self.__strains[int(v)] for v in line.split()])
            panmixia.partitioned_shuffle(substrains)
         else:
            panmixia.shuffle(self.__strains)


   def get_strains(self):
      return self.__strains

   def get_aagen(self):
      return self.__aagen

   def get_genenames(self):
      return self.__genenames

