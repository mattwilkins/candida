
import numpy as np
import misc.common

class Aagen:
   """Generates Amino Acid sequences."""

   def __init__(self, codon_to_aa):
      """Form an Amino Acid generator.

      codon_to_aa:   dict of codon string to aminoacid char

      """

      # convert to a dict of 3-tuple to aminoacid char
      self.__codon_to_aa = {}
      for c, a in codon_to_aa.iteritems():
         self.__codon_to_aa[tuple(misc.common.nucs2np(c))] = a


   def amino_acids(self, seq):
      """seq is a list of ints, map to aminoacid sequence."""
      if len(seq) == 3:
         try:
            self.__codon_to_aa[(seq[0], seq[1], seq[2])]
         except:
            print misc.common.np2nucs(seq)

         return self.__codon_to_aa[(seq[0], seq[1], seq[2])]
      else:
         return [self.__codon_to_aa[(seq[i], seq[i+1], seq[i+2])] for i in range(0, len(seq), 3)]

   def get_aa(self):
      """Return all the aminoacids."""
      return set(self.__codon_to_aa.values())


   def get_codons(self, aa):
      """Return codons for aa."""
      codons = []
      for c, a in self.__codon_to_aa.iteritems():
         if a == aa:
            codons.append(c)
      return codons
