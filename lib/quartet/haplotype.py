
import random
import misc.common

nuc_to_other_nucs = {}
for k, v in {'A' : ['C', 'G', 'T'], 'C' : ['A', 'G', 'T'], 'G' : ['A', 'C', 'T'], 'T' : ['A', 'C', 'G']}.iteritems():
   nuc_to_other_nucs[ord(k)] = map(ord, v)

class Haplotype:
   """A Haplotype."""

   def __init__(self, lofarrays):
      """Form a haplotype.

      lofarrays   -- a list of numpy arrays, possibly only one

      """
  
      # make sure we have atleast one allele
      if len(lofarrays) < 1:
         raise ValueError, "Need data for atleast one Allele"

      # check alleles are same length
      lengths = [len(a) for a in lofarrays]
      if not misc.common.const(lengths):
         raise ValueError, "Allele data are not the same length"

      self.size = lofarrays[0].size
      self.__height = len(lofarrays)
      self.__alleles = lofarrays

   def get_ambiguities(self):
      """Sites where the alleles are different."""

      ambig = []
      for i in range(self.size):
         val = self.__alleles[0][i]
         for a in self.__alleles:
            if a[i] != val:
               ambig.append(i)
               continue
      return ambig

   def get_height(self):
      return self.__height

   def copy(self):
      na = []
      for a in self.__alleles:
         na.append(a.copy())
      return Haplotype(na)

   def mutate(self, prob, test = False):
      """Mutate the alleles."""
      for a in self.__alleles:
         a.mutate(prob, test)

   def copy_into(self, another):
      """Copy myself into the other haplotyp."""
      for a, b in zip(self.__alleles, another.get_alleles()):
         a.copy_into(b)

   def get_allele(self):
      if self.__height == 1:
         return self.__alleles[0]
      else:
         return random.sample(self.__alleles, 1)[0]

   def get_alleles(self):
      return self.__alleles

   def __iter__(self):
      for a in self.__alleles:
         yield a

