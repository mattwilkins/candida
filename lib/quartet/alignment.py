
import misc.common
import numpy as np
import re
from allele import Allele




class Alignment:

   """A number of strains or alleles

   If there are 7 genes labelled a-g, and n strains we'll make

      [ a0  b0  c0  d0  e0  f0  g0  ]
      [ a0' b0' c0' d0' e0' f0' g0' ]
      [  .   .   .   .   .   .   .
      [  .   .   .   .   .   .   .
      [ an  bn  cn  dn  en  fn  gn  ]
      [ an' bn' cn' dn' en' fn' gn' ]

   where (ai, ai') is the haplotype of the ith strain.  If collapse is True
   though we will collapse each strain first, an example would be

      [ a0  b0' c0  d0' e0' f0' g0  ]
      [  .   .   .   .   .   .   .
      [  .   .   .   .   .   .   .
      [ an' bn  cn' dn' en  fn' gn  ]

   If the strains are really just alleles just form

      [ a0 ]
      [ a1 ]
      [ a2 ]
      [ .  ]
      [ .  ]
      [ an ]

   """

   def __init__(self, strains, collapse=False):
      """Form the alignment."""

      if collapse:
         height = len(strains)
      else:
         height = sum([s.get_height() for s in strains])
      width = strains[0].size

      self.__align = np.empty((height, width), dtype=np.uint8)
      self.shape = self.__align.shape

      # simple case.  assume if first strain is allele, all are
      if type(strains[0]) is Allele:
         for i, s in enumerate(strains):
            self.__align[i, :] = s
         return

      # must be real strains, have to deal with haplotypes etc
      row = 0
      for s in strains:
         base = 0

         for hap in s:

            # one or many alleles
            if collapse:
               alleles = [hap.get_allele()]
            else:
               alleles = hap.get_alleles()
            
            # stack alleles below each other
            for i, allele in enumerate(alleles):
               self.__align[row + i, base:(base+hap.size)] = allele
            base += hap.size
        
         # jump down number of rows we have added
         row += (i + 1)


   def __get_silent_codons(self, aagen):
      """codons in each strain that map to same aminoacids."""
      
      # An array with False in it at the noisy codons,so can just do a
      # compress which removes where condition is False

      # amino acids for each strain in a matrix
      #  aa = [
      #           [R, F, S, ...]
      #           [C, F, U, ...]
      #           [R, T, S, ...]
      #           [V, F, S, ...]
      #        ]
      
      silent = []
      # go triple by triple
      for j in range(0, self.__align.shape[1], 3):

         # aa[i] = amino acids for column i
         aa = []
         for triple in self.__align[:,j:j+3]:
            aa.append(aagen.amino_acids(triple))
         if misc.common.const(aa):
            silent.extend([True] * 3)
         else:
            silent.extend([False] * 3)

      return silent


   def rm_noisy_codons(self, aagen):
      """Remove codons in each strain that map to different aminoacids."""

      mask = self.__get_silent_codons(aagen)
      self.__align = np.compress(mask, self.__align, axis=1)
      self.shape = self.__align.shape

#   def rm_noisy_codons_and_ambiguities(self, aagen):
#
#      # array that is True at silent codons
#      silents = self.__get_silent_codons(aagen)
#
#      # FIXME, clumsy a set with indices where ambiguities occur
#      ambigs = set()
#      for s in self.__strains:
#         ambigs.update(set(s.get_ambiguity_sites()))
#
#      # FIXME, clumst convert ambigs to a 
#      mask = []
#      for i in range(len(silents)):
#         if silents[i] == False or (i in ambigs):
#            mask.append(False)
#         else:
#            mask.append(True)
#     
#      self.__align = np.compress(mask, self.__align, axis=1)
   
   #def __getslice__(self, i, j):
   #   return self.__align.__getslice__(i, j)

   def __getitem__(self, i):
      return self.__align.__getitem__(i)

   def __setitem__(self, i, j):
      return self.__align.__setitem__(i, j)

   def __iter__(self):
      for row in self.__align:
         yield row

   def h_variability(self):
      """Works out H for each site.

      H = 1 - sum(proportion(nuc)^2)

      where proportion(nuc) is the proportion of nuc at this site.  Eg

            A
            A
            C
            T
            G
            G
            G
      H = 1 - (2/7)^2 - (1/7)^2 - (1/7)^2 - (3/7)^2
      """
    
      # variability gives us something like: If there are three nucs,
      # 40% A's, 50% C's and 10% G's, [0.5, 0.4, 0.1]

      hs = []
      for colsummary in self.variability():
         h = 1
         for p in colsummary:
            h -= p*p
         hs.append(h)
      return hs
      

   def variability_summary(self):
      """Summary of variability of sites.

      A list where the ith element is a list giving the proportion of
      sites that have (i+1) unique nucleotides, and the proportion of
      the most popular nuc.  Eg [[0.6, 1], [0.3, 0.9], [0.1,0.4]]
      would indicate that 60% of sites are constant (only one
      nucleotide); 30% have two nucs with those two nucs being in the
      radio 90% 10%; and 10% have three different nucleotides and on
      average within each site the most popular nuc appears with
      frequency 40%

      """
      
      # maps length of each self.variability to array of proportion of
      # most popular nuc.  for instance len2popular[1] with be a
      # constant array of all 1's, since for constant sites (len = 1)
      # there is only one nuc; len2popular[2] will be an array that
      # contains the proportion of the most popular nuc for sites that
      # have two unique nucs.
      len2popular = {}

      for a in self.variability():
         
         if len(a) not in len2popular:
            len2popular[len(a)] = [a[0]]
         else:
            len2popular[len(a)].append(a[0])


      # put into val [len, mean]
      val = []
      for k in sorted(len2popular.keys()):
         l = 1.0 * len(len2popular[k])
         mean = sum(len2popular[k])/l
         val.append([l/self.__align.shape[1], mean])

      return val

   def variability(self):
      """Proportion of different nucleotides at each site.

      A python array with the ith element being a sorted list containing
      the proportion of nucleotides in the ith column.  For instance
      if column i only has one nuc, then the list is [1].
      If there are two nucs, say 80% T's and 20% G's, [0.8, 0.2]
      If there are three nucs, 40% A's, 50% C's and 10% G's,
      [0.5, 0.4, 0.1]

      """

      # number of nucs in any column
      tot = 1.0 * self.__align.shape[0]

      # fill column by column
      var = []
      for j in range(self.__align.shape[1]):
         column =  self.__align[:,j]
       
         count = {}
         for n in column:
            if n not in count:
               count[n] = 1
            else:
               count[n] += 1
          
         # number of nucs sorted max first
         b = sorted(count.values(), reverse=True)

         # proportion
         var.append(map(lambda (x): x/tot, b))

      return var


   def transitions(self):
      """Return the number of transitions occur.
      
      A transition occurs when a site has C and T in it, or A and G in
      it.

      There cannot be a transition in a constant site.
      
      In a site with three nucleotides it is impossible to know if
      there was a transition or not, since A could have gone to C gone
      to G (all transversions), or A -> G, and G -> C (one transition
      and one transversion).  I assume there has been exactly one
      (because the logic to check is easier).
     
      We have the same problem with a site with four nucleotides,
      there could have been 0, 1, or 2 transitions.  I assume there
      has been exactly two (because the logic to check is easier).

      """
      t = 0
      for j in range(self.__align.shape[1]):
         column =  self.__align[:,j]

         nucs = misc.common.frequency_of_elements(column)
         if ord('C') in nucs and ord('T') in nucs:
            t += 1
         if ord('A') in nucs and ord('G') in nucs:
            t += 1

      return t

   def transversions(self):
      """Return a list showing where transversions occur.
      
      A transition is a site with a A and C, A and T, G and C, or G
      and T in it.

      There cannot be a transversion in a constant site.
     
      We have the same problem with determining the number of
      transversions in sites with 3 and 4 nucleotides as above in the
      transitions method.  Again I don't complicate the code and in
      the 3 nucleotide case there will end up being 2 transversions,
      and in the 4 nucleotide case there will end up being 4
      transversions.

      """
      t = 0
      for j in range(self.__align.shape[1]):
         column =  self.__align[:,j]

         nucs = misc.common.frequency_of_elements(column)
         if ord('A') in nucs and ord('C') in nucs:
            t += 1
         if ord('A') in nucs and ord('T') in nucs:
            t += 1
         if ord('G') in nucs and ord('C') in nucs:
            t += 1
         if ord('G') in nucs and ord('T') in nucs:
            t += 1

      return t



