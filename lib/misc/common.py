
import numpy as np
import math

def roundup(x, base=3):
   # round up to a multiple of base
   return int(base * math.ceil(float(x)/base))

def rounddown(x, base=3):
   # round down to a multiple of base
   return int(base * math.floor(float(x)/base))

def nucs2np(s):
   """Converts a string, or a list, of nucleotides, to numpy array."""
   return np.array([ord(x) for x in s],dtype=np.uint8)

def np2nucs(a):
   """Numpy array of uint8 to list of nucleotides."""
   return map(chr, a)

def np2str(a):
   """Numpy array of uint8 to string of nucleotides."""
   return ''.join(np2nucs(a))

def const(ls):
   """Are the elements in ls all the same?."""

   if len(ls) == 0:
      return 1

   first = ls[0]
   for x in ls:
      if x != first:
         return 0

   return 1

def frequency_of_elements(ls):
   """A dict mapping elements in list to frequency of occurance."""
   
   f = {}
   for i in ls:
      if i in f:
         f[i] += 1
      else:
         f[i] = 1
   return f

def most_popular(ls):
   """A list that contains the most popular values in given list."""
   freq = frequency_of_elements(ls)
   maximum = 0
   popular = []
   for el, f in freq.iteritems():
      if f > maximum: 
         popular = [el]
         maximum = f
      elif f == maximum:
         popular.append(el)
   return popular
  

def flatten(x):
    """flatten(sequence) -> list

    Returns a single, flat list which contains all elements retrieved
    from the sequence and all recursively contained sub-sequences
    (iterables).

    Examples:
    >>> [1, 2, [3,4], (5,6)]
    [1, 2, [3, 4], (5, 6)]
    >>> flatten([[[1,2,3], (42,None)], [4,5], [6], 7, MyVector(8,9,10)])
    [1, 2, 3, 42, None, 4, 5, 6, 7, 8, 9, 10]"""

    result = []
    for el in x:
        #if isinstance(el, (list, tuple)):
        if hasattr(el, "__iter__") and not isinstance(el, basestring):
            result.extend(flatten(el))
        else:
            result.append(el)
    return result

def int2binary(n, width = 7):
   """integer to binary string with given width."""
   return [(n >> y) & 1 for y in range(width-1, -1, -1)]

