#!/usr/bin/python2.6

import os
import sys
import random
import string
import itertools
import subprocess
import operator
from optparse import OptionParser, OptionValueError
import ConfigParser
import numpy as np
import textwrap as tw


# so we can find our ../lib no matter how we are called
findbin = os.path.dirname(os.path.realpath(sys.argv[0]))
sys.path.append(findbin + '/../lib')

# parse command line
p = OptionParser()
p.add_option("-c", action="store_true", dest="onecodon", help="Sequences have one codon per amino acid")
p.add_option("-m", action="store_true", dest="markup", help="Do all sequences, but * sequences that have one codon per amino acid, and put a + beside the most irregular")
p.add_option("-a", action="store_true", dest="justaa", help="Just print the amino acid")
p.add_option("-r", action="store_true", dest="ranges", help="Just print the varscore ranges")
(opts, args) = p.parse_args()


#random.seed(0)
c2aa = {'CTT': 'L', 'ATG': 'M', 'ACA': 'T', 'ACG': 'T', 'ATC': 'I', 'AAC': 'N', 'ATA': 'I', 'AGG': 'R', 'CCT': 'P', 'ACT': 'T', 'AGC': 'S', 'AAG': 'K', 'AGA': 'R', 'CAT': 'H', 'AAT': 'N', 'ATT': 'I', 'CTG': 'S', 'CTA': 'L', 'CTC': 'L', 'CAC': 'H', 'TGG': 'W', 'CCG': 'P', 'AGT': 'S', 'CCA': 'P', 'CAA': 'Q', 'CCC': 'P', 'TAT': 'Y', 'GGT': 'G', 'TGT': 'C', 'CGA': 'R', 'CAG': 'Q', 'TCT': 'S', 'GAT': 'D', 'CGG': 'R', 'TTT': 'F', 'TGC': 'C', 'GGG': 'G', 'TAG': '*', 'GGA': 'G', 'TAA': '*', 'GGC': 'G', 'TAC': 'Y', 'TTC': 'F', 'TCG': 'S', 'TTA': 'L', 'TTG': 'L', 'TCC': 'S', 'ACC': 'T', 'TCA': 'S', 'GCA': 'A', 'GTA': 'V', 'GCC': 'A', 'GTC': 'V', 'GCG': 'A', 'GTG': 'V', 'GAG': 'E', 'GTT': 'V', 'GCT': 'A', 'AAA': 'K', 'TGA': '*', 'GAC': 'D', 'CGT': 'R', 'GAA': 'E', 'CGC': 'R'}

# read in the strain
strain = sys.stdin.readline().strip()

# make sure strain is a multiple of three in length
strain = strain[0: 3 * (len(strain)/ 3)]

# split the strain into an array of codons
codons = []
for i in range(0, len(strain), 3):
   codons.append(strain[i:i+3])

# make the amino acid array
aa = []
for c in codons:
   aa.append(c2aa[c])
if opts.justaa:
   print ''.join(aa)
   sys.exit(0)


# get the scores
cmd = os.path.join(findbin, 'similar_sequences -nf')
cmd = "echo %s | %s" % (strain, cmd)
if opts.onecodon: cmd += ' -c '
cmd += ' | /awc/local/bin/repeat_chars -s | /awc/local/bin/varscore.pl -s'
p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
(stdout, stderr) = p.communicate()
results = stdout.split('\n')
results.pop(-1)  # the last line has a \n, so we get a blank result

# maps seq to varscore
seq = {}
for r in results:
   (sid, foo0, foo1, foo2, varscore, reducedsequence) = r.split()
   seq[reducedsequence] = float(varscore)

# if we want to * the results that were obtained by doing the one codon per amino acid, we need to rerun to get those
if opts.markup:
   cmd = os.path.join(findbin, 'similar_sequences -cnf')
   cmd = "echo %s | %s" % (strain, cmd)
   cmd += ' | /awc/local/bin/repeat_chars -s | /awc/local/bin/varscore.pl -s'
   p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   (stdout, stderr) = p.communicate()
   results = stdout.split('\n')
   results.pop(-1)  # the last line has a \n, so we get a blank result
   onecodonseq = {}
   for r in results:
      (sid, foo0, foo1, foo2, varscore, reducedsequence) = r.split()
      onecodonseq[reducedsequence] = float(varscore)

   cmd = os.path.join(findbin, 'similar_sequences -mf')
   cmd = "echo %s | %s" % (strain, cmd)
   cmd += ' | /awc/local/bin/repeat_chars -s | /awc/local/bin/varscore.pl -s'
   p = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
   (stdout, stderr) = p.communicate()
   results = stdout.split('\n')
   results.pop(-1)  # the last line has a \n, so we get a blank result
   if len(results) > 0:
      (sid, foo0, foo1, foo2, varscore, maxirg) = results.pop(0).split()
      maxcodonseq = {maxirg : float(varscore)}
   else:
      maxirg = 'notthere'

# only display the min and max
if opts.ranges:
   print "Varscores from %.3g to %.3g with lengths %d to %d" % (min(seq.itervalues()), max(seq.itervalues()), min(map(len, seq.iterkeys())), max(map(len, seq.iterkeys())))
   if opts.markup:

      min_len = min(map(len, onecodonseq.iterkeys()))
      max_len = max(map(len, onecodonseq.iterkeys()))
      print "One codon varscores from %.3g to %.3g with lengths %d to %d" % (min(onecodonseq.itervalues()), max(onecodonseq.itervalues()), min_len, max_len)
      seqs_with_similar_len = filter(lambda (s): min_len <= len(s) <= max_len, seq.iterkeys())
      print "Varscores of sequences with same length from %.3g to %.3g" % (min(map(seq.get, seqs_with_similar_len)), max(map(seq.get, seqs_with_similar_len)))

      # is the onecodon stuff close to the max?
      if max(onecodonseq.itervalues()) >= max(map(seq.get, seqs_with_similar_len)):
         print "One-codon is at the max"
      else:
         print "One-codon is NOT at the max"

      if maxirg == 'notthere':
         print "Max irregularity sequence had no varscore"
      else:
         print "Max irregularity varscore is %.3g with length %d" % (maxcodonseq[maxirg], len(maxirg))
         seqs_with_similar_len = filter(lambda (s): len(s) == len(maxirg), seq.iterkeys())
         print "Varscores of sequences with same length from %.3g to %.3g" % (min(map(seq.get, seqs_with_similar_len)), max(map(seq.get, seqs_with_similar_len)))
         # is the max iregular close to the min?
         if maxcodonseq[maxirg] <= min(map(seq.get, seqs_with_similar_len)):
            print "Max irregularity seq is at the min"
         else:
            print "Max irregularity seq is NOT at the min"



else:

   # now display
   for s, score in seq.iteritems():
      if opts.markup:
         if s in onecodonseq:
            print score, '*', s
         elif s == maxirg:
            print score, '+', s
         else:
            print score, ' ', s
      else:
         print score, s
   
   
   
   
