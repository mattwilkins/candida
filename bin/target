#!/usr/bin/env python
#
# reads in x and y values, puts a least squares line through them,
# then finds where it crosses the target
#

import os
import sys
from optparse import OptionParser
import numpy as np

# parse command line
p = OptionParser(usage="usage: %prog [options] <comma separated pair> ... \nFinds where 'least squares' line crosses given target")
p.add_option("-t", action="store", type="float", dest="target", help="What y value we are looking for")
p.add_option("-c", action="store", type="float", dest="check", help="Print x value of pair (x,y) if y is within +- this fraction of target")

p.set_defaults(target = 0.0)
(opts, args) = p.parse_args()

# get the x's and y's
xs = []
ys = []
if len(args) < 2:
   print "Need atleast two pairs"
   sys.exit(2)
for pair in args:
   x, y = pair.split(',')
   xs.append(float(x))
   ys.append(float(y))
if len(xs) != len(ys):
   print "We have", len(xs), "x values, and", len(ys), "y values"
   sys.exit(3)

xs = np.array(xs)
ys = np.array(ys)

# solve y = mx + c using instructions on
#
#  http://docs.scipy.org/doc/numpy/reference/generated/numpy.linalg.lstsq.html?highlight=least squares
#
A = np.vstack([xs, np.ones(len(xs))]).T
m, c = np.linalg.lstsq(A, ys)[0]

if opts.check:
   lower = opts.target * (1 - opts.check)
   upper = opts.target * (1 + opts.check)
   for x, y in zip(xs, ys):
      if y > lower and y < upper:
         print "Target is", opts.target, "this point is close (", x, y, ")"
else:
   print (opts.target - c)/m

