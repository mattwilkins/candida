#!/usr/bin/env python

import os
import sys
import ConfigParser
import random
import string
from time import strftime
from optparse import OptionParser, OptionValueError


# so we can find our ../lib no matter how we are called
findbin = os.path.dirname(os.path.realpath(sys.argv[0]))
sys.path.append(findbin + '/../lib')

import numstuff.varscore
from quartet.allele import Allele
from quartet.quartet import Quartet
from quartet.strain import Strain
from quartet.setup import Setup
from numstuff.prob import expected_recomb_index
import misc.common
import numpy as np

# -p is for panmixia, -m is to define a mix file, can't do both at same time
def check_p_and_m_not_both_used(option, opt_str, value, parser):
   if option.dest == 'mixia_file' and parser.values.panmixia:
      raise OptionValueError, "Can't use -m since -p already specified"
   elif option.dest == 'panmixia' and parser.values.mixia_file:
      raise OptionValueError, "Can't use -p since -m already specified"
   setattr(parser.values, option.dest, (value or True))

# parse command line
p = OptionParser()
p.add_option("-s", action="store",   dest="strains_file", help="The file containing the strains info")
p.add_option("-a", action="store",   dest="c2aa_file",  help="Codon to Amino Acid file")
p.add_option("-i", action="store",   type="int", dest="iterations",   help="Number of iterations to perform")
p.add_option("-p", action="callback",dest="panmixia",     callback=check_p_and_m_not_both_used, help="Do Panmixia")
p.add_option("-m", action="callback",dest="mixia_file", type="str", callback=check_p_and_m_not_both_used, help="File containing mix sets")
p.add_option("-d", action="store",   type="str", dest="dump_file",  help="File to put strains (collapsed) in phylip format")
p.add_option("-l", action="store_true", dest="label", help="Label output with what quartet produced it")
p.add_option("-k", action="store_true", dest="keepnonsilent", help="Keep, don't throw out, the nonsilent codons")
p.add_option("-e", action="store_true", dest="extrada", help="Print da_allsites on end of line")
p.set_defaults(strains_file = findbin + '/../code/data/strains')
p.set_defaults(c2aa_file = findbin + '/../code/data/c2aa')
p.set_defaults(iterations   = 10000000)
p.set_defaults(panmixia     = False)
(opts, args) = p.parse_args()

#random.seed(0)

# read in the data
if opts.mixia_file:
   mixia = open(opts.mixia_file)
else:
   mixia = opts.panmixia 
setup = Setup(opts.strains_file, opts.c2aa_file, mixia)
strains = setup.get_strains()

# maybe dump out the strains
if opts.dump_file:
   dump = open(opts.dump_file, 'w')
   # header is number of strains, and length
   strains[0].collapse()
   dump.write(str(len(strains)) + ' ' + str(len(strains[0].list())) + "\n")
   for s in strains:
      s.collapse()
      dump.write( ("%-10s" % s.get_name()) + s.str() + "\n" )
   dump.close()

#import psyco
#psyco.full()

for i in range(int(opts.iterations)):
   s = random.sample(strains, 4)
   quartet = Quartet(s)

   if not opts.keepnonsilent:
      quartet.rm_noisy_codons(setup.get_aagen())

   # ignore if not enough informative sites
   ninf = quartet.num_inf_sites()
   if ninf < 2:
      continue

   da = quartet.divergence_age()
   ri = quartet.recomb_index()
   print da, ri/expected_recomb_index(ninf),


   if opts.label:
      print '# ' + ' '.join(map(lambda x: x.get_name(), s))
   elif opts.extrada:
      print quartet.divergence_age_allsites()
   else:
      print

   sys.stdout.flush()


