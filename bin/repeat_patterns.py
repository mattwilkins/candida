#!/usr/bin/python


import cgi
import cgitb
cgitb.enable(display=1, logdir="/tmp")

import os, sys

form = cgi.FieldStorage()

# header
print "content-type: text/html\n"
print '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">'
print '<html><head><title>Patterns</title>'
print '<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">'
print '</head><body><h1>Patterns</h1>'

def error_die(str):
   print str
   print '</body>'
   print '</html>'
   sys.exit(1)

if form.has_key('debug'):
   debug = True
else:
   debug = False
def dprint(str):
   if debug: print str + '<br>'

def save_file(fileitem):
   """Save this fileitem into a temp filename."""
  
   # better be a file
   if not fileitem.file: raise ValueError, "This ain't a file"

   fout = tempfile.NamedTemporaryFile()
   while 1:
      chunk = fileitem.file.read(100000)
      if not chunk: break
      fout.write (chunk)
   fout.seek(0)

   return fout

def process_file(fh):
   """Process the given file."""

   # open up
   try:
      im = Image.open(fh)
   except IOError:
      error_die("This isn't a picture!")
   (w, h) = im.size
   dprint("The image (width, height) = " + str(w) + ', ' + str(h) + ") and mode = " + im.mode)
   if debug:
      im.save('tmp/0.png')
      print "<img src='tmp/0.png'/><br>"

   # remove 10% to get rid of edge effects
   box = (int(0.1*w), int(0.1*h), int(0.9*w), int(0.9*h))
   im = im.crop(box)
   (w, h) = im.size
   dprint("After chopping the boarder: (width, height) = " + str(w) + ', ' + str(h) + ") and mode = " + im.mode)
   if debug:
      im.save('tmp/1.png')
      print "<img src='tmp/1.png'/><br>"

   # should be a strip up and down
   if w > h:
      print "Picture needs rotating, please consider sending in portrait orientation<br>"
      im = im.rotate(270)
      (w, h) = im.size
      dprint("After rotating: (width, height) = " + str(w) + ', ' + str(h) + ") and mode = " + im.mode)
      if debug:
         im.save('tmp/2.png')
         print "<img src='tmp/2.png'/><br>"

   # convert to grey
   dprint("Converting to grey scale")
   im = im.convert("L")
   if debug:
      im.save('tmp/3.png')
      print "<img src='tmp/3.png'/><br>"

   # average each row
   average = []
   col = 0
   for x in im.getdata():
      tot += x
      col += 1
      if col == w:
         average.push(1.0*tot/w)
         tot = col = 0

if form.has_key('filename'):
   try:
      fh = save_file(form['filename'])
   except ValueError:
      error_die("Couldn't upload, doesn't appear to be a file")

   process_file(fh)

else:
   print '<form action="' + os.environ['SCRIPT_NAME'] + '" method="POST" enctype="multipart/form-data">'
   print '<p>For a given C. Albican input sequence, this programme
   will calculate the Amino Acid sequence, and (using the codons given
   in the input sequence) all possible sequences that map to the same
   Amino Acid sequence.  For example, suppose you enter ACGTACACAT for
   your sequence.  The Amino Acid sequence is TYT, and there are four
   possible patterns: ACATACACA, ACATACACG, ACGTACACA, and ACGTACACG.'
   print '<div align="center"><br>'
   print '<input type="radio" name="group1" value="Raw"> Just list the sequences one after another on new lines<br>'
   print '<input type="radio" name="group1" value="Relaxed FASTA" checked> Relaxed FASTA, but not wrapped at 80 columns<br>'
   print '<input type="radio" name="group1" value="FASTA"> Proper FASTA, this is slow'
   print '</div>'
   print 'Sequence: <input name="filename" type="file"><br>'
   print 'Turn on debugging: <input name="debug" type="checkbox"/>'
   print '<br>'
   print '<input name="submit" type="submit" value="Submit"/>'
   print '</form>'

# footer
print '</body>'
print '</html>'
   



