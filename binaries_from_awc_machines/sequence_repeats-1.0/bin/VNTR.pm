package VNTR;

sub new
{
 my ($class)=@_;
 bless{
     _start=>$_[1],
     _end=>$_[2],
     _period=>$_[3],
     _exponent=>$_[4],
     _sizeConsensus=>$_[5],
     _match=>$_[6],
     _indel=>$_[7],
     _score=>$_[8],
     _A=>$_[9],
     _C=>$_[10],
     _G=>$_[11],
     _T=>$_[12],
     _entropy=>$_[13],
     _consensus=>$_[14],
     _sequence=>$_[15],
     _name=>$_[16],
     _id=>$_[17]
     }, $class;
}

1;
