#!/usr/bin/perl -w
use strict;
use IPC::Open2;
use FindBin;
use lib("$FindBin::Bin");
use VNTR;


### run TRF
system("trf400.linux.exe $ARGV[0] 2 5 5 80 10 40 500 -d -h > /dev/null") or die "Cannot execute TRF\n";

### read results
my @repeats=@{&readRepeats("$ARGV[0].2.5.5.80.10.40.500.dat")};
unlink("$ARGV[0].2.5.5.80.10.40.500.dat") or die "Cannot remove $ARGV[0].2.5.5.80.10.40.500.dat\n";

### initialize model variables
my $beta=-1.83336667859624590000;
my $sigma=12.91549665014885500000;

my $pid=open2(\*OR,\*OW,"octave") or die "Cannot launch Octave\n";

#print OW "LOADPATH=['../octaveFunctions/:',LOADPATH];\n";
print OW "addpath('/awc/local/lib/pkg/sequence_repeats-1.0/octaveFunctions')\n";
print OW "format long\n";
print OW "alpha=dlmread(\"/awc/local/lib/pkg/sequence_repeats-1.0/data/alpha\",\"\\t\")\n";
print OW "labels=dlmread(\"/awc/local/lib/pkg/sequence_repeats-1.0/data/labels\",\"\\t\")\n";
print OW "matrix=dlmread(\"/awc/local/lib/pkg/sequence_repeats-1.0/data/rawMatrix\",\"\\t\")\n";
print OW "matrixNorm=[ ((matrix(:,1)-(mean(matrix(:,1))))/std(matrix(:,1))) ((matrix(:,2)-(mean(matrix(:,2))))/std(matrix(:,2))) ((matrix(:,3)-(mean(matrix(:,3))))/std(matrix(:,3))) ]\n";
print OW "sigma=".$sigma."\n";
print OW "beta=".$beta."\n";

my $line;
while(defined($line=<OR>)&&($line!~/^beta/)){}

### Calculate VARscore and output repeats

foreach my $repeat (@repeats){
    print OW "S=[ ($repeat->{_match}-mean(matrix(:,1)))/std(matrix(:,1)) ($repeat->{_period}-mean(matrix(:,2)))/std(matrix(:,2)) ($repeat->{_exponent}-mean(matrix(:,3)))/std(matrix(:,3)) ]\n";
    print OW "varscore=alpha'*(labels.*exp(-sum((matrixNorm-ones(320,1)*S).^2,2)/sigma))+beta\n";
    while(defined($line=<OR>)&&($line!~/^varscore/)){}
    $line=~/^varscore\s*\=\s*(.+)/ or die "pb with $line";
    my $varscore=$1;

    print $repeat->{_id}."\t".$varscore."\t".$repeat->{_name}."\t".$repeat->{_start}."\t".$repeat->{_end}."\t".$repeat->{_period}."\t".$repeat->{_exponent}."\t".$repeat->{_sizeConsensus}."\t".$repeat->{_match}."\t".$repeat->{_indel}."\t".$repeat->{_score}."\t".$repeat->{_A}."\t".$repeat->{_C}."\t".$repeat->{_G}."\t".$repeat->{_T}."\t".$repeat->{_entropy}."\t".$repeat->{_consensus}."\t".$repeat->{_sequence}."\n";
}

close(OW);
close(OR);


sub readRepeats {
    my $file=shift;
    my @repeats=();
    my $sequenceName;
    open(F,"<$file") or die "Cannot read $file\n";
    my $i=0;
    while(<F>){
        chomp;
        if(/Sequence:\s*(\S+)/){
            $sequenceName=$1;
        }
        elsif(/^\d+\s+\d+/){
            my @tmp=split(/\s+/,$_);push(@tmp,$sequenceName);
	    $i++;push(@tmp,$i);
            my $repeat=VNTR->new(@tmp);
            if(($repeat->{_period}>1)&&(!($repeat->{_A}==100)||($repeat->{_C}==100)||($repeat->{_G}==100)||($repeat->{_T}==100))){
                push(@repeats,$repeat);
            }
        }
    }
    close(F);
    return(\@repeats);
}
