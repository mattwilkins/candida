#!/bin/bash
#
#

function usage_exit()
{
   cat << EOF
Usage:
   
   $0 [-v] [-p] [-s] 

will read fasta sequences from stdin and print out the varscores found

   -v be verbose
   -p just show the percentage of sequences with no repeat charateristics
   -s just show the min and max summary of the varscores

EOF
   exit 1
}

# read the -p or -s
doingpercent=0
doingsummary=0
verbose=0
while getopts "vps" Option ; do
   case $Option in
      v ) 
         verbose=1
         ;;
      p ) 
         doingpercent=1
         ;;
      s ) 
         doingsummary=1
         ;;
      * )
         usage_exit
   esac
done
shift $(($OPTIND - 1))

# get the stdin into a file, we need this so we can count the number
# of sequences
cat - > /tmp/$$.seq

# run repeat_chars on the input fasta
cat /tmp/$$.seq | /awc/local/lib/pkg/sequence_repeats-1.0/bin/repeat_chars -sn > /tmp/$$.repeatcharout

# did we want the percentage that didn't have repeats?
if [ $doingpercent -eq '1' ] ; then
   tot=`grep "^>" /tmp/$$.seq | wc -l`
   trffailed=`grep 'no repeat signature' /tmp/$$.repeatcharout | wc -l | cut -d\  -f1`
   echo "$trffailed/$tot" | bc -l

   rm /tmp/$$.seq /tmp/$$.repeatcharout
   exit
fi

# now we need to get the varscores
grep -v 'no repeat signature' /tmp/$$.repeatcharout | /awc/local/bin/varscore.pl -s > /tmp/$$.varscores
perl -pi -e 's/\S+\s+\S+\s+\S+\s+\S+\s+(\S+)\s.*/$1/' < /tmp/$$.varscores > /tmp/$$.justvarscores

# maybe just want the min and max
if [ $doingsummary -eq '1' ] ; then
   if [ -s /tmp/$$.justvarscores ] ; then
      cat /tmp/$$.justvarscores | ~/bin/minimum
      cat /tmp/$$.justvarscores | ~/bin/maximum
   else
      echo "No varscores to get min and max of"
   fi
elif [ $verbose -eq '1' ] ; then
   cat /tmp/$$.varscores
else
   cat /tmp/$$.justvarscores
fi

rm /tmp/$$.seq /tmp/$$.repeatcharout /tmp/$$.varscores /tmp/$$.justvarscores

