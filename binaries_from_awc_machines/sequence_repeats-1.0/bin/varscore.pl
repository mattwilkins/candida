#!/usr/bin/env perl
#
#
use strict;
use warnings;
use IPC::Open2;
use Getopt::Std;
use FindBin;

my %opts;
sub vprint
{
   my $msg = shift;
   print $msg if ( $opts{'v'} );
}

sub usage
{
   print
   "Usage:
         $0 [-v] [-s] [-h] ...

   Reads stdin, or given files, in the format
      <unit size> <purity> <number of units> [<sequence>]

   And prints out the 
      <number> <purity> <unit size> <number of units> <variablity> [<sequence>]

   -v    verbose
   -s    print the sequence at the end of the line
   -h    display this help

";

   exit;
}

getopts('vsh', \%opts);
usage if ( $opts{'h'} );

my $i=0;
my %repeat;
for (<>){
    chomp;
    my @tmp=split;
    if ( $opts{'s'} ) {
      die "Need four elements in line $_\n" if ( scalar @tmp != 4 );
   } else {
      die "Need three elements in line $_\n" if ( scalar @tmp != 3 );
   }

   $i++;
   $repeat{$i}->{period}=$tmp[0];
   $repeat{$i}->{period}=~s/\%//g;
   $repeat{$i}->{purity}=$tmp[1];
   $repeat{$i}->{purity}=~s/\%//g;
   $repeat{$i}->{exponent}=$tmp[2];
   $repeat{$i}->{sequence}=$tmp[3] if $opts{'s'};
}

### initialize model variables
my $beta=-1.83336667859624590000;
my $sigma=12.91549665014885500000;

### read model data files
my $pid=open2(\*OR,\*OW,"/awc/local/bin/octave") or die "Cannot launch Octave\n";
print OW "addpath('/awc/local/lib/pkg/sequence_repeats-1.0/octaveFunctions')\n";
print OW "format long\n";
print OW "alpha=dlmread(\"/awc/local/lib/pkg/sequence_repeats-1.0/data/alpha\",\"\\t\")\n";
print OW "labels=dlmread(\"/awc/local/lib/pkg/sequence_repeats-1.0/data/labels\",\"\\t\")\n";
print OW "matrix=dlmread(\"/awc/local/lib/pkg/sequence_repeats-1.0/data/rawMatrix\",\"\\t\")\n";
print OW "matrixNorm=[ ((matrix(:,1)-(mean(matrix(:,1))))/std(matrix(:,1))) ((matrix(:,2)-(mean(matrix(:,2))))/std(matrix(:,2))) ((matrix(:,3)-(mean(matrix(:,3))))/std(matrix(:,3))) ]\n";
print OW "sigma=".$sigma."\n";
print OW "beta=".$beta."\n";

my $line;
while(defined($line=<OR>)&&($line!~/^beta/)){}

### Calculate VARscore and output repeats

foreach my $key (sort({$a<=>$b} keys(%repeat))){
    print OW "S=[ ($repeat{$key}->{purity}-mean(matrix(:,1)))/std(matrix(:,1)) ($repeat{$key}->{period}-mean(matrix(:,2)))/std(matrix(:,2)) ($repeat{$key}->{exponent}-mean(matrix(:,3)))/std(matrix(:,3)) ]\n";
    print OW "varscore=alpha'*(labels.*exp(-sum((matrixNorm-ones(320,1)*S).^2,2)/sigma))+beta\n";
    while(defined($line=<OR>)&&($line!~/^varscore/)){}
    $line=~/^varscore\s*\=\s*(.+)/ or die "pb with $line";
    my $varscore=$1;
    
    print $key."\t".$repeat{$key}->{purity}."\t".$repeat{$key}->{period}."\t".$repeat{$key}->{exponent}."\t$varscore";
    print ' ' . $repeat{$key}->{sequence} if $opts{'s'};
    print "\n";
}
close(OW);
close(OR);


