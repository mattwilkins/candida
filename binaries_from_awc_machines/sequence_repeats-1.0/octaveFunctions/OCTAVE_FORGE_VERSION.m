## OCTAVE_FORGE_VERSION The release date of octave-forge, as integer YYYYMMDD
function v=OCTAVE_FORGE_VERSION
  v=20060709;
endfunction
